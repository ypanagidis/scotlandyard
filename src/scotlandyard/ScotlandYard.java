package scotlandyard;

import graph.Edge;
import graph.Node;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;

/**
 * A class to perform all of the game logic.
 */

public class ScotlandYard implements ScotlandYardView, Receiver {

    public LinkedHashMap<Colour, PlayerData> players = new LinkedHashMap<>();
    public Integer numberOfDetectives;
    public List<Boolean> rounds;
    public ScotlandYardGraph graph;
    protected MapQueue<Integer, Token> queue;
    protected Integer gameId;
    protected Random random;

    public Integer mrXLastKnownLocation = 0;
    public PlayerData currentPlayer;
    public ArrayList<PlayerData> playerDatas = new ArrayList<>();
    ArrayList<Spectator> spectators = new ArrayList<>();


    public Integer round = 0;
    public boolean mrXWon = false;
    public boolean detectivesWon = false;
    /**
     * Constructs a new ScotlandYard object. This is used to perform all of the game logic.
     *
     * @param numberOfDetectives the number of detectives in the game.
     * @param rounds the List of booleans determining at which rounds Mr X is visible.
     * @param graph the graph used to represent the board.
     * @param queue the Queue used to put pending moves onto.
     * @param gameId the id of this game.
     */
    public ScotlandYard(Integer numberOfDetectives, List<Boolean> rounds, ScotlandYardGraph graph, MapQueue<Integer, Token> queue, Integer gameId) {
        this.queue = queue;
        this.gameId = gameId;
        this.random = new Random();
        this.numberOfDetectives = numberOfDetectives;
        this.rounds = rounds;
        this.graph = graph;
    }

    /**
     * Starts playing the game.
     */
    public void startRound() {
        if (isReady() && !isGameOver()) {
            turn();
        }
    }

    /**
     * Notifies a player when it is their turn to play.
     */
    public void turn() {
        Integer token = getSecretToken();
        queue.put(gameId, new Token(token, getCurrentPlayer(), System.currentTimeMillis()));
        notifyPlayer(getCurrentPlayer(), token);
    }

    public void shouldMrXReveal(){
        if (rounds.get(round)){mrXLastKnownLocation = players.get(Colour.Black).getLocation();}
    }

    /**
     * Plays a move sent from a player.
     *
     * @param move the move chosen by the player.
     * @param token the secret token which makes sure the correct player is making the move.
     */
    public void playMove(Move move, Integer token) {
        Token secretToken = queue.get(gameId);
        if (secretToken != null && token.equals(secretToken.getToken())) {
            queue.remove(gameId);
            play(move);
            nextPlayer();
            startRound();
        }
    }

    /**
     * Returns a random integer. This is used to make sure the correct player
     * plays the move.
     * @return a random integer.
     */
    private Integer getSecretToken() {
        return random.nextInt();
    }

    /**
     * Notifies a player with the correct list of valid moves.
     *
     * @param colour the colour of the player to be notified.
     * @param token the secret token for the move.
     */
    private void notifyPlayer(Colour colour, Integer token) {
        Player player;
        player = players.get(colour).getPlayer();
        player.notify(players.get(colour).getLocation(), validMoves(colour), token, this);

    }

    /**
     * Passes priority onto the next player whose turn it is to play.
     */
    protected void nextPlayer() {
        int indexOfNextPlayer = playerDatas.indexOf(currentPlayer) + 1;
        if (playerDatas.size() >indexOfNextPlayer){
            this.currentPlayer = playerDatas.get(indexOfNextPlayer);
        }
        if (indexOfNextPlayer == playerDatas.size()){
            this.currentPlayer = players.get(Colour.Black);
        }
    }

    /**
     * Allows the game to play a given move.
     *
     * @param move the move that is to be played.
     */
    protected void play(Move move) {
        if (move instanceof MoveTicket) play((MoveTicket) move);
        else if (move instanceof MoveDouble) play((MoveDouble) move);
        else if (move instanceof MovePass) play((MovePass) move);
    }

    /**
     * Plays a MoveTicket.
     *
     * @param move the MoveTicket to play.
     */
    protected void play(MoveTicket move) {
        currentPlayer.setLocation(move.target);
        currentPlayer.removeTicket(move.ticket);

        if (move.colour == Colour.Black){
            shouldMrXReveal();
            round++;
        } else{
            players.get(Colour.Black).addTicket(move.ticket);
        }

        if (move.colour.equals(Colour.Black) && !(rounds.get(round))) {
            move = MoveTicket.instance(move.colour, move.ticket, mrXLastKnownLocation);
        }
        specNotify(move);

    }

    /**
     * Plays a MoveDouble.
     *
     * @param move the MoveDouble to play.
     */
    protected void play(MoveDouble move) {
        Move move2;

        players.get(move.colour).removeTicket(Ticket.Double);
        if (!rounds.get(round)) {move2 = MoveDouble.instance(move.colour, move.move1.ticket, move.move1.target, move.move2.ticket, mrXLastKnownLocation); specNotify(move2);}
        specNotify(move);


        play(move.move1);
        play(move.move2);
    }

    /**
     * Plays a MovePass.
     *
     * @param move the MovePass to play.
     */
    protected void play(MovePass move) {

        if(currentPlayer.getColour().equals(Colour.Black)){
            shouldMrXReveal();
            round++;
        }
        specNotify(move);
    }

    /**
     * Creates a list of all the locations of all the players
     * Does not include Mr X's location
     *
     * Returns ArrayList<Integer>
     */

    public ArrayList<Integer> findPlayerLocations(Colour player){
        ArrayList<Integer> playerLocations = new ArrayList<>();

        for (Map.Entry<Colour, PlayerData> entry: players.entrySet()){
            if (entry.getValue().getColour() != player){
                if (entry.getValue().getColour() != Colour.Black){
                    playerLocations.add(entry.getValue().getLocation());
                }
            }
        }
        return playerLocations;
    }

    /*
     * Calculates all the edges
     * From the node the player is currently on
     *
     * Takes Colour player
     * Returns list of edges.
     */
    public ArrayList<Edge<Integer, Transport>> edgeCalc(Colour player){
        ArrayList<Edge<Integer, Transport>> edges = new ArrayList<>();

        for (Node<Integer> node: graph.getNodes()){
            if (players.get(player).getLocation().equals(node.getIndex())){
                List<Edge<Integer, Transport>> temp = new ArrayList<>(graph.getEdgesFrom(node));
                edges.addAll(temp);
            }
        }
        return edges;
    }

    /*
     * Modifies the list of edges
     * Removes the edges that as a target have a node
     * At which there is a detective.
     */
    public void edgesMod(ArrayList<Edge<Integer, Transport>> edges, ArrayList<Integer> playerLocations){

        for (Edge<Integer, Transport> edge: new ArrayList<Edge<Integer, Transport>>(edges)){
            for (Integer loc: playerLocations){
                if (Objects.equals(loc, edge.getTarget().getIndex())){
                    edges.remove(edge);
                }
            }
        }
    }

    /**
     * Returns the list of valid moves for a given player.
     *
     * @param player the player whose moves we want to see.
     * @return the list of valid moves for a given player.
     */
    public List<Move> validMoves(Colour player) {

        /**
         Collections:
         moves = list of valid moves we return.
         moveTickets: list of all tickets that are made into moves.
         playerLocations: list of all locations of players (int)
         edges:list of app[pll edges connected to src node.
         */

        ArrayList<Move> moves = new ArrayList<Move>();
        ArrayList<MoveTicket> moveTickets = new ArrayList<>();
        ArrayList<Integer> playerLocations;
        ArrayList<Edge<Integer, Transport>> edges;

        //Ticket test
        boolean taxi = false;
        boolean bus = false;
        boolean underground = false;
        boolean secret = false;
        boolean dmove = false;

        if (players.get(player).getTickets().get(Ticket.Taxi) != 0) {
            taxi = true;
        }
        if (players.get(player).getTickets().get(Ticket.Bus) != 0) {
            bus = true;
        }
        if (players.get(player).getTickets().get(Ticket.Underground) != 0) {
            underground = true;
        }
        if (players.get(player).getTickets().get(Ticket.Secret) != 0) {
            secret = true;
        }
        if (players.get(player).getTickets().get(Ticket.Double) != 0) {
            dmove = true;
        }

        playerLocations = findPlayerLocations(player);

        //edge list creation
        edges = edgeCalc(player);

        //player location test
        edgesMod(edges, playerLocations);

        //move list creation (aka: ValidMoves)
        for (Edge edge : edges) {

            if (taxi && edge.getData().equals(Transport.Taxi)) {
                moveTickets.add(MoveTicket.instance(player, Ticket.Taxi, (Integer) edge.getTarget().getIndex()));
            }

            if (bus && edge.getData().equals(Transport.Bus)) {
                moveTickets.add(MoveTicket.instance(player, Ticket.Bus, (Integer) edge.getTarget().getIndex()));
            }

            if (underground && edge.getData().equals(Transport.Underground)) {
                moveTickets.add(MoveTicket.instance(player, Ticket.Underground, (Integer) edge.getTarget().getIndex()));
            }

            if (secret && edge.getData().equals(Transport.Boat)) {
                moveTickets.add(MoveTicket.instance(player, Ticket.Secret, (Integer) edge.getTarget().getIndex()));
            }

            if (secret) {
                moveTickets.add(MoveTicket.instance(player, Ticket.Secret, (Integer) edge.getTarget().getIndex()));
            }
        }

        moves.addAll(moveTickets);


        if ((moves.size() == 0) && (player != Colour.Black)) {
            moves.add(MovePass.instance(player));
        }

        //double move
        if (dmove) {
            taxi = players.get(player).getTickets().get(Ticket.Taxi) > 1;

            bus = players.get(player).getTickets().get(Ticket.Bus) > 1;

            underground = players.get(player).getTickets().get(Ticket.Underground) > 1;

            secret = players.get(player).getTickets().get(Ticket.Secret) > 1;

            for (MoveTicket moveTicket : moveTickets) {
                Node<Integer> nodeSrc = graph.getNode(moveTicket.target);

                ArrayList<Edge<Integer, Transport>> newEdges = new ArrayList<>(graph.getEdgesFrom(nodeSrc));

                edgesMod(newEdges, playerLocations);

                for (Edge edge : newEdges) {
                    if (taxi && edge.getData().equals(Transport.Taxi)) {
                        if (!moves.contains(MoveDouble.instance(player, moveTicket, MoveTicket.instance(player, Ticket.Taxi, (Integer) edge.getTarget().getIndex())))) {
                            moves.add(MoveDouble.instance(player, moveTicket, MoveTicket.instance(player, Ticket.Taxi, (Integer) edge.getTarget().getIndex())));
                        }
                    }

                    if (bus && edge.getData().equals(Transport.Bus)) {
                        if (!moves.contains(MoveDouble.instance(player, moveTicket, MoveTicket.instance(player, Ticket.Bus, (Integer) edge.getTarget().getIndex())))) {
                            moves.add(MoveDouble.instance(player, moveTicket, MoveTicket.instance(player, Ticket.Bus, (Integer) edge.getTarget().getIndex())));
                        }
                    }

                    if (underground && edge.getData().equals(Transport.Underground)) {
                        if (!moves.contains((MoveDouble.instance(player, moveTicket, MoveTicket.instance(player, Ticket.Underground, (Integer) edge.getTarget().getIndex()))))) {
                            moves.add(MoveDouble.instance(player, moveTicket, MoveTicket.instance(player, Ticket.Underground, (Integer) edge.getTarget().getIndex())));
                        }
                    }

                    if (secret && edge.getData().equals(Transport.Boat)) {
                        if (!moves.contains(MoveDouble.instance(player, moveTicket, MoveTicket.instance(player, Ticket.Secret, (Integer) edge.getTarget().getIndex())))) {
                            moves.add(MoveDouble.instance(player, moveTicket, MoveTicket.instance(player, Ticket.Secret, (Integer) edge.getTarget().getIndex())));
                        }
                    }

                    if (secret) {
                        if (!moves.contains(MoveDouble.instance(player, moveTicket, MoveTicket.instance(player, Ticket.Secret, (Integer) edge.getTarget().getIndex())))) {
                            moves.add(MoveDouble.instance(player, moveTicket, MoveTicket.instance(player, Ticket.Secret, (Integer) edge.getTarget().getIndex())));
                        }
                    }

                }
            }
        }

        return moves;
    }
    /**
     * Allows spectators to join the game. They can only observe as if they
     * were a detective: only MrX's revealed locations can be seen.
     *
     * @param spectator the spectator that wants to be notified when a move is made.
     */
    public void spectate(Spectator spectator) {
        spectators.add(spectator);
    }

    public void specNotify(Move move){

        for (Spectator spectator : spectators){
            spectator.notify(move);
        }

    }

    /**
     * Allows players to join the game with a given starting state. When the
     * last player has joined, the game must ensure that the first player to play is Mr X.
     *
     * @param player the player that wants to be notified when he must make moves.
     * @param colour the colour of the player.
     * @param location the starting location of the player.
     * @param tickets the starting tickets for that player.
     * @return true if the player has joined successfully.
     */
    public boolean join(Player player, Colour colour, int location, Map<Ticket, Integer> tickets) {
        if (isReady()){return false;}
        PlayerData gamePlayer = new PlayerData(player, colour, location, tickets);
        players.put(colour, gamePlayer);
        playerDatas.add(gamePlayer);
        if (colour.equals(Colour.Black)){
            currentPlayer = gamePlayer;
        }
        return true;
    }

    /**
     * A list of the colours of players who are playing the game in the initial order of play.
     * The length of this list should be the number of players that are playing,
     * the first element should be Colour.Black, since Mr X always starts.
     *
     * @return The list of players.
     */
    public List<Colour> getPlayers() {
        return new ArrayList<Colour>(players.keySet());
    }

    /**
     * Returns the colours of the winning players. If Mr X it should contain a single
     * colour, else it should send the list of detective colours
     *
     * @return A set containing the colours of the winning players
     */
    public Set<Colour> getWinningPlayers() {
        HashSet<Colour> winPlayers = new HashSet<>();

        if (isGameOver()){
            if (detectivesWon){
                for (PlayerData playerData : playerDatas){
                    if (!(playerData.getColour().equals(Colour.Black))){
                        winPlayers.add(playerData.getColour());
                    }
                }
                return winPlayers;
            }

            if (mrXWon){
                winPlayers.add(Colour.Black);
                return winPlayers;
            }
        }
        return new HashSet<Colour>();
    }

    /**
     * The location of a player with a given colour in its last known location.
     *
     * @param colour The colour of the player whose location is requested.
     * @return The location of the player whose location is requested.
     * If Black, then this returns 0 if MrX has never been revealed,
     * otherwise returns the location of MrX in his last known location.
     * MrX is revealed in round n when {@code rounds.get(n)} is true.
     */
    public int getPlayerLocation(Colour colour) {
        shouldMrXReveal();
        if (colour.equals(Colour.Black)){
            return mrXLastKnownLocation;
        }
        return players.get(colour).getLocation();
    }

    /**
     * The number of a particular ticket that a player with a specified colour has.
     *
     * @param colour The colour of the player whose tickets are requested.
     * @param ticket The type of tickets that is being requested.
     * @return The number of tickets of the given player.
     */
    public int getPlayerTickets(Colour colour, Ticket ticket) {
        return players.get(colour).tickets.get(ticket);
    }

    /**
     * The game is over when MrX has been found or the agents are out of
     * tickets. See the rules for other conditions.
     *
     * @return true when the game is over, false otherwise.
     */
    public boolean isGameOver() {
        ArrayList<PlayerData> detectives = new ArrayList<>();
        boolean doesntHaveMoves = true;

        if(!isReady()){return false;}

        for (PlayerData player : playerDatas){
            if (player.getColour() != Colour.Black){
                detectives.add(player);
            }
        }

        for (PlayerData detective : detectives) {
            if (doesItContainMrX()){
                if (detective.getLocation().equals(players.get(Colour.Black).getLocation())){
                    detectivesWon = true;
                    return true;
                }
            }
        }

        for (PlayerData player : detectives){
            if (doesntHaveMoves) {
                if (validMoves(player.getColour()).size() == 1){
                    ArrayList<Move> temp = new ArrayList<>(validMoves(player.getColour()));
                    doesntHaveMoves = temp.contains(MovePass.instance(player.getColour()));
                } else {
                    doesntHaveMoves = false;
                }
            }
        }

        ArrayList<Move> mrXMoves = new ArrayList<>(validMoves(Colour.Black));

        if ((mrXMoves.size() == 0)){
            detectivesWon = true;
            return true;
        }

        if (doesntHaveMoves){
            mrXWon = true;
            return true;
        }

        if ((round + 1 == rounds.size()) && (currentPlayer.getColour().equals(Colour.Black))){
            mrXWon = true;
            return true;
        }


        return false;
    }

    private boolean doesItContainMrX(){
        for (PlayerData player : playerDatas){
            if (player.getColour().equals(Colour.Black)){
                return true;
            }
        }

        return false;
    }


    /**
     * A game is ready when all the required players have joined.
     *
     * @return true when the game is ready to be played, false otherwise.
     */
    public boolean isReady() {
        if (players.size() -1 == this.numberOfDetectives){return true;}
        return false;
    }

    /**
     * The player whose turn it is.
     *
     * @return The colour of the current player.
     */
    public Colour getCurrentPlayer() {
        return currentPlayer.getColour();
    }

    /**
     * The round number is determined by the number of moves MrX has played.
     * Initially this value is 0, and is incremented for each move MrX makes.
     * A double move counts as two moves.
     *
     * @return the number of moves MrX has played.
     */
    public int getRound() {return this.round;}

    /**
     * A list whose length-1 is the maximum number of moves that MrX can play in a game.
     * The getRounds().get(n) is true when MrX reveals the target location of move n,
     * and is false otherwise.
     * Thus, if getRounds().get(0) is true, then the starting location of MrX is revealed.
     *
     * @return a list of booleans that indicate the turns where MrX reveals himself.
     */
    public List<Boolean> getRounds() {
        return rounds;
    }

}
